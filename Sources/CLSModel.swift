//
//  CLSModel.swift
//  CouchbaseLiteSwift
//
//  Created by Marco Betschart on 19.09.16.
//  Copyright © 2016 MANDELKIND. All rights reserved.
//

import Foundation
import CouchbaseLite


/**
Generic AnyModel class which provides convenience functions
for your own Model classes.
*/
open class CLSModel: CBLModel{
	
	///Convenience function to create a new model
	open class func create() throws -> Self{
		return try self.create( CLSDatabase.standard )
	}
	
	
	///Convenience function to create a new model using an existing database connection
	open class func create(_ connection: CBLDatabase) -> Self{
		return self.init(forNewDocumentIn: connection)
	}
	
	
	///Convenience function to create a new model using an existing database
	open class func create(_ database: CLSDatabase) throws -> Self{
		return self.create(try database.connection())
	}
	
	
	///Convenience function to create a new model and populate it with the given properties
	open class func create(_ from: [String: AnyObject?]) throws -> Self{
		return try self.create(CLSDatabase.standard,from: from )
	}
	
	
	///Convenience function to create a new model using a given database and populate it with properties
	open class func create(_ database: CLSDatabase, from: [String: AnyObject?]) throws -> Self{
		return self.create(try database.connection(), from: from)
	}
	
	
	///Convenience function to create a new model using a given database connection and populate it with properties
	open class func create(_ connection: CBLDatabase, from: [String: AnyObject?]) -> Self{
		let model = self.init(forNewDocumentIn: connection)
		
		for (property,value) in from{
			model.setValue(value, ofProperty: property)
		}
		
		return model
	}
	
	
	///Convenience function to load an existing model
	open class func load(_ docID: String) throws -> Self?{
		return try self.load(CLSDatabase.standard, withID: docID)
	}
	
	
	///Convenience function to load an existing model from the given database
	open class func load(_ database: CLSDatabase, withID docID: String) throws -> Self?{
		return self.load(try database.connection(), withID: docID)
	}
	
	
	///Convenience function to load an existing model using the given database connection
	open class func load(_ connection: CBLDatabase, withID docID: String) -> Self?{
		if let doc = connection.document(withID: docID){
			return self.init(for: doc)
		}
		
		return nil
	}
	
	
	///Type of the model as String
	open override func getValueOfProperty(_ property: String) -> Any? {
		if property == "type" && super.getValueOfProperty(property) == nil {
			return type(of: self).valueOfTypeProperty() as AnyObject?
		}
		
		return super.getValueOfProperty(property)
	}
	
	
	open class func valueOfTypeProperty() -> String?{
		return NSStringFromClass(self)
	}
	
	
	open override func propertiesToSave() -> [AnyHashable: Any] {
		var properties = super.propertiesToSave()
		properties["type"] = type(of: self).valueOfTypeProperty()
		
		for(key, value) in properties{
			if value is Double ||
				value is Int ||
				value is Float ||
				value is UInt ||
				value is Int8 ||
				value is UInt8 ||
				value is Int16 ||
				value is UInt16 ||
				value is Int32 ||
				value is UInt32 ||
				value is Int64 ||
				value is UInt64{
				
				properties[key] = value as! NSNumber
				//@see https://developer.apple.com/library/content/documentation/Swift/Conceptual/BuildingCocoaApps/WorkingWithCocoaDataTypes.html
			}
		}
		
		return properties
	}
	
	
	open override func isEqual(_ object: Any?) -> Bool {
		if let model = object as? CLSModel, let id = model.document?.documentID, let type = model.type{
			return id == self.document?.documentID && type == self.type
		}
		
		return super.isEqual(object)
	}
	
	
	fileprivate var _attachmentsDidLoad = false
	fileprivate var _attachments: [CLSAttachment]? = nil
	
	
	///All attachments of the model
	open var attachments: [CLSAttachment]?{
		get{
			if !self._attachmentsDidLoad{
				self._attachments = loadAttachments()
				self._attachmentsDidLoad = true
			}
			
			return self._attachments
		}
		set(newAttachments){
			if let newAttachments = newAttachments{
				if let oldAttachments = self._attachments{
					for oldAttachment in oldAttachments{
						if newAttachments.index(of: oldAttachment) == nil{
							oldAttachment.removeAsAttachment()
						}
					}
				}
				
				for newAttachment in newAttachments{
					newAttachment.model = self
				}
			}
			saveAttachments(newAttachments) //maybe this should better be called in willSave()...?
			
			self._attachmentsDidLoad = true
			self._attachments = newAttachments
		}
	}
	
	
	///Function can be overriden to exclude specific attachments for example
	open func loadAttachments() -> [CLSAttachment]?{
		if let attachmentNames = self.attachmentNames{
			return attachmentNames.flatMap{
				if let attachment = self.attachmentNamed($0){
					return CLSAttachment(model: self){
						$0.attachmentIdentifier = attachment.name
						
						$0.attachmentName = attachment.name
						$0.attachmentContent = attachment.content
						$0.attachmentContentType = attachment.contentType
					}
				}
				
				return nil
			}
		}
		
		return nil
	}
	
	
	open func saveAttachments(_ attachments: [CLSAttachment]?){
		if let newAttachments = attachments{
			for newAttachment in newAttachments{
				newAttachment.saveAsAttachment()
			}
		}
	}
}


public protocol CLSAttachmentType: Equatable {
	var attachmentIdentifier: String? { get set }
	var attachmentType: String? { get set }
	
	var attachmentName: String? { get set }
	var attachmentContentType: String? { get set }
	var attachmentContent: Data? { get set }
	
	func saveAsAttachment()
	func removeAsAttachment()
}


/**
Generic CLSAttachment class which provides a interface,
to customize the behaviour of specific custom Attachments.
*/
open class CLSAttachment: NSObject,CLSAttachmentType{
	var model: CLSModel?
	
	open var attachmentIdentifier: String?
	open var attachmentType: String?
	
	open var attachmentName: String?
	open var attachmentContentType: String?
	open var attachmentContent: Data?
	
	
	public required init(model: CLSModel?, configure: ( (CLSAttachment) -> Void )?){
		self.model = model
		super.init()
		configure?(self)
	}
	
	
	open func saveAsAttachment(){
		if let identifier = self.attachmentIdentifier , self.attachmentName != identifier{
			self.model?.removeAttachmentNamed(identifier)
		}
		
		if let name = self.attachmentName, let contentType = self.attachmentContentType, let content = self.attachmentContent{
			self.model?.setAttachmentNamed(name, withContentType: contentType, content: content)
		}
		
		self.attachmentIdentifier = self.attachmentName
	}
	
	
	open func removeAsAttachment(){
		if let identifier = self.attachmentIdentifier{
			self.model?.removeAttachmentNamed(identifier)
		}
	}
	
	
	open func copyWithZone(_ zone: NSZone?) -> AnyObject { // Support for NSCopying
		return type(of: self).init(model: self.model){
			$0.attachmentIdentifier = self.attachmentIdentifier
			$0.attachmentType = self.attachmentType
			
			$0.attachmentName = self.attachmentName
			$0.attachmentContentType = self.attachmentContentType
			$0.attachmentContent = self.attachmentContent
		}
	}
}


public func == (lhs: CLSAttachment, rhs: CLSAttachment) -> Bool{
	return lhs.model == rhs.model && lhs.attachmentIdentifier == rhs.attachmentIdentifier
}
